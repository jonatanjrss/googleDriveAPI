#! /usr/bin/env python

# Python 3 - drive API v3


from oauth2client import file

from apiclient.discovery import build

from httplib2 import Http

from decouple import config


STORAGE_JSON = config('STORAGE_JSON')
CLIENT_SECRET = config('CLIENT_SECRET')
SCOPES = 'https://www.googleapis.com/auth/drive'


def _get_credentials():
    try:
        import argparse
        flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
    except ImportError:
        flags = None
    store = file.Storage(STORAGE_JSON)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(
            CLIENT_SECRET, scope=SCOPES)
        credentials = tools.run_flow(flow, store, flags)\
                    if flags else tools.run(flow, store)
return build('drive', 'v3', http=credentials.authorize(Http()))


# Cria diretório
def mkdir(name, parent=[]):
    if not _find(name):
        SCOPES = 'https://www.googleapis.com/upload/drive/v3/files'
        drive_service = _get_credentials()      
        folder=drive_service.files().create(
            body={'name':name,
                  'parents':parent,
                    'mimeType':'application/vnd.google-apps.folder'},
            fields='id').execute()
        return { name: folder.get('id')}
    else:
        print(
            "Não foi possível criar o diretório '{}': \
pois ele já existe".format(name))

        
# lista diretórios
def lsdir(parent='root'):
    SCOPES = 'https://www.googleapis.com/drive/v3/files'
    drive_service = _get_credentials()
    
    folders = drive_service.files().list(
    q="'{}' in parents and mimeType='application/vnd.google-apps.folder' and trashed!=True".format(parent),
    fields='nextPageToken, files(id, name)').execute()
    for folder in folders.get('files', []):
        print(folder.get('name'), folder.get('id'), sep='--->')


#lista arquivos
def lsfile(parent='root'):
    SCOPES = 'https://www.googleapis.com/drive/v3/files'
    drive_service = _get_credentials()
    
    files = drive_service.files().list(
    pageSize=1000,
    q="'{}' in parents and mimeType!='application/vnd.google-apps.folder' and trashed!=True".format(parent),
    fields='nextPageToken, files(id, name)').execute()
    for file in files.get('files', []):
        print(file.get('name'), file.get('id'))


# Deleta arquivos
def delete(name):
    f=_find(name)
    _delete(f[0].get('id'))


def _find(name):
    drive_service = _get_credentials()
    files = drive_service.files().list(
        q="name='{}'  and trashed!=True".format(name),
        fields='nextPageToken, files(id, name)').execute()
    return files.get('files', [])


def _delete(fileId):
    drive_service = _get_credentials()
    drive_service.files().delete(fileId=fileId).execute()
